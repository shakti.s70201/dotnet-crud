﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DtoEmployee
    {
        //[Required]
        //public Guid? EmployeeId { get; set; }
        
        [Required, StringLength(50, ErrorMessage = "The Modifier value cannot exceed 50 characters")]
        public string? EmployeeName { get; set; }
        public int? EmployeeAge { get; set; }

        [Required, StringLength(50, ErrorMessage = "The Description value cannot exceed 50 characters")]

        public string? EmployeeEmail { get; set; }

        public long? EmployeeSalary { get; set; }
        //public bool? IsActive { get; set; }
        //[Required]
        public Guid? OperationBy { get; set; }
    }
}
