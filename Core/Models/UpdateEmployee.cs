﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class UpdateEmployee
    {
        [Required]
        public Guid? EmployeeId { get; set; }
        
        public string? EmployeeName { get; set; }
        public int? EmployeeAge { get; set; }
        public string? EmployeeEmail { get; set; }

        public long? EmployeeSalary { get; set; }

        //public DateTime UpdatedOn { get; set; }

        [Required] 
        public Guid? UpdatedBy { get; set; }
    }
}
