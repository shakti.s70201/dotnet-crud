﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class ListParameters
    {
        public int Start { get; set; }
        
        public int PageSize { get; set; }

        public string? SortCol { get; set; }

        public string? SearchKey { get; set; }

        public class ResponseList
        {
            public string? Message { get; set; }

            public int TotalRecords { get; set; }

            public List<EmployeeList>? Data { get; set; }

            public string? Status { get; set; }

            public class EmployeeList
            {
                public Guid EmployeeId { get; set; }
                public string? EmployeeName { get; set; }
                public int EmployeeAge { get; set; }
                public string? EmployeeEmail { get; set; }
                public long? EmployeeSalary { get; set; }
                public string? CreatedOn { get; set; }
                public Guid? CreatedBy { get; set; }
                public string? DeletedOn { get; set; }
                public Guid? DeletedBy { get; set; }
                public string? UpdatedOn { get; set; }
                public Guid? UpdatedBy { get; set; }
            }
        }
    }
}

