﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class GetEmployee
    {
        public string? EmployeeName { get; set; }
        public int? EmployeeAge { get; set; }
        public string? EmployeeEmail { get; set; }
        public long? EmployeeSalary { get; set; }
    }
}
