﻿using Core;
using Core.Models;
using DotNetAssignment.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using static Core.Models.ListParameters;

namespace DotNetAssignment.Controllers
{
    [Route("api/manager/")]
    [ServiceFilter(typeof(ValidationFilterAttribute))]
    public class ManagerController : Controller
    {
        private readonly IManagerRepository _managerRepository;
        public ManagerController(IManagerRepository managerRepository)
        {
            _managerRepository = managerRepository;
        }


        [HttpPost]
        public async Task<ActionResult> Add([FromBody] DtoEmployee employee)
        {
            
            var response = await _managerRepository.AddAsync(employee);

            return Ok(response);
        }


        [HttpGet]
        public async Task<IActionResult> Get(string employeeId)
        {
            GetEmployee response = await _managerRepository.GetEmployee(employeeId).ConfigureAwait(false);

            return Ok(response);
        }


        [HttpPut]
        public async Task<ActionResult> Update([FromBody] UpdateEmployee update)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(new
            //    {
            //        Status = false,
            //        Message = string.Join(Environment.NewLine, ModelState.Values
            //                                .SelectMany(x => x.Errors)
            //                                .Select(x => x.ErrorMessage))
            //    });
            //}
            var response = await _managerRepository.UpdateAsync(update);

            return Ok(response);
        }



        [HttpDelete]
        public async Task<ActionResult> Delete([FromBody] DeleteEmployee deleteEmployee)
        {
           
            var response = await _managerRepository.DeleteAsync(deleteEmployee);

            return Ok(response);
        }



        [HttpGet,Route("GetList")]
        public IActionResult GetList(ListParameters listParameter)
        {
            ResponseList result = _managerRepository.GetAllEmployeeList(listParameter);
            return Ok(result);
        }
    }
}
