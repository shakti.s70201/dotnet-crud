﻿using Core;
using Core.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using static Core.Models.ListParameters;
using static Core.Models.ListParameters.ResponseList;

namespace Infrastructure
{
    public class ManagerRepository : IManagerRepository
    {
        private readonly IConfiguration _configuration;

        private static string _connectionString = string.Empty;
        public ManagerRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration["ConnectionStrings:Manager"]!;
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }

        public async Task<Response> AddAsync(DtoEmployee employee)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[USPtblEmployee]", employee, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

            return response;
        }

        public async Task<GetEmployee> GetEmployee(string Id)
        {
            GetEmployee response;

            using (IDbConnection db = Connection)
            {
                response = await db.QueryFirstOrDefaultAsync<GetEmployee>("[uspEmployeeData]", Id, commandType: CommandType.StoredProcedure).ConfigureAwait(false);
            }

            return response;
        }
        public async Task<Response> UpdateAsync(UpdateEmployee update)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[uspEmployeeDataUpdate]", update, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

            return response;
        }

        public async Task<Response> DeleteAsync(DeleteEmployee deleteEmployee)
        {

            Response response;

            using (IDbConnection db = Connection)
            {
                response = await db.QueryFirstOrDefaultAsync<Response>("[uspEmployeeDelete]", deleteEmployee, commandType: CommandType.StoredProcedure);
            }

            return response;
        }

        public ResponseList GetAllEmployeeList(ListParameters listParameter)
        {
            ResponseList response;
            using IDbConnection db = Connection;
            var result = db.QueryMultiple("[uspGetAllEmployee]", listParameter, commandType: CommandType.StoredProcedure);


            response = result.Read<ResponseList>().FirstOrDefault()!;
            response!.Data = result.Read<EmployeeList>().ToList();
            response.TotalRecords = result.Read<int>().FirstOrDefault();
            return response;


        }

    }
}