﻿using Core.Models;
using static Core.Models.ListParameters;

namespace Core
{
    public interface IManagerRepository
    {
        Task<Response> AddAsync(DtoEmployee employee);

        Task<Response> DeleteAsync(DeleteEmployee DeleteEmployee);

        Task<GetEmployee> GetEmployee(string employeeId);

        Task<Response> UpdateAsync(UpdateEmployee update);

        ResponseList GetAllEmployeeList(ListParameters listParameter);
        
    }
}