﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DeleteEmployee
    {
        public Guid EmployeeId { get; set; }

        public DateTime DeletedOn { get; set; }

        public Guid DeletedBy { get; set; }
    }
}
